# 1597

> Pontos: 100
> Resolução: Mayfly

```
> ... as in https://xkcd.com/1597/

http://git.ritsec.club:7000/1597.git/

~knif3
```

```
> ... como em https://xkcd.com/1597/

http://git.ritsec.club:7000/1597.git/

~knif3
```

## O Problema

Quando clonamos o repositório na nossa máquina (`git clone http://git.ritsec.club:7000/1597.git/`), vemos um arquivo `flag.txt` vazio.

## A Solução

Checando as branches a gente vê que tem duas:

```bash
git branch
```

Output:

```
  !flag
* master
```

Indo para a outra branch e lendo o arquivo conseguimos a flag.

```bash
git checkout '!flag'
cat flag.txt
```

Output:

```
RS{git_is_just_a_tre3_with_lots_of_branches}
```

