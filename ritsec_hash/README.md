# RITSEC Hash

> Pontos: 250
> Resolução: Mayfly

```
> Hmmm.. we found this hash along with a white paper explaining this custom hashing algorithm.

Can you break it for us?

hash : 435818055906

Flag should be submitted as RS{<cracked hash>}

Author: 1nv8rZim
```

```
> Hmmm.. encontramos esse hash junto de um artigo explicando esse algoritmo de hashing customizado.

Você consegue quebra-lo para nós?

hash : 435818055906

A flag deve ser enviada como RS{<hash craqueado>}

Autor: 1nv8rZim
```

## O Problema

Foi dado um hash ('435818055906') e um pdf explicando uma função de hash. Após [implementar o que foi descrito no pdf](ritsec_hash.cpp) conseguimos rodar contra a lista [`rockyou.txt`](rockyou.txt):

```bash
cat rockyou.txt | while read LINHA
do
	RES=$(./hash $LINHA)
	if [ "$RES" == "$HASH" ]
	then
		echo $LINHA
		exit
	fi
done
```

OUTPUT:

```
invaderzim
```

Para obter a flag:

```
RS{invaderzim}
```

