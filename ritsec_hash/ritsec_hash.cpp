#include <iostream>
#include <vector>
#include <iomanip>


std::vector<uint8_t> Hash(std::vector<uint8_t> h, uint8_t i)
{
	std::vector<uint8_t> res_h(6);
	for (auto r = 0; r != 13; ++r) {
		res_h[1] = h[0];
		res_h[2] = h[3]<<2;
		res_h[3] = h[1]>>5;
		res_h[4] = h[0] + h[5];
		res_h[5] = h[3];

		res_h[0] = ((h[2]^h[4])&h[5]) + h[1] + res_h[2] + i + r;

		h = res_h;
	}

	return res_h;
}

int main(int argc, char** argv)
{
	if (argc != 2) return 1;
	std::string m(argv[1]);
	std::vector<uint8_t> H = {'R','I','T','S','E','C'};
	for (char c : m) {
		H = Hash(H, c);
	}

	std::cout << std::setfill('0');
	for (uint8_t i : H) {
		std::cout << std::hex << std::setw(2) << static_cast<int>(i);
	}
	std::cout << std::endl;
}

