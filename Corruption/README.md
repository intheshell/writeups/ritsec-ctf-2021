# Corruption

> Pontos: 500
> Resolução: Mayfly

```
It seems that this remote is somehow corrupted. See if we can somehow get the data...

git://git.ritsec.club:9418/corruption.git

~knif3
```

```
Parece que este remoto está, de alguma maneira, corrúpto. Veja se conseguimos pegar os dados de algum jeito...

git://git.ritsec.club:9418/corruption.git

~knif3
```

## O problema

O primeiro instinto foi tentar clonar o repositório com `git clone git://git.ritsec.club:9418/corruption.git`, mas como o texto indica, um erro ocorre e o repositório não é clonado.

```
Cloning into 'corruption'...
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (2/2), done.
remote: aborting due to possible repository corruption on the remote side.
fatal: early EOFs:  80% (4/5)
fatal: fetch-pack: invalid index-pack output
```

## A solução

A solução foi inicializar um repositório git e registrar o repositório do desafio como repositório remoto.

```bash
mkdir corruption; cd corruption
git init
git remote add origin git://git.ritsec.club:9418/corruption.git
```

Depois, verifiquei quais eram os objetos que estavam registrados nesse repositório remoto (isso poderia ter sido feito antes de criar o repositório local).

```bash
git ls-remote origin
# ou
# git ls-remote git://git.ritsec.club:9418/corruption.git
# se quiser fazer antes do passo anterior
```

Output:

```
f7fe593ca04a5d2a835d0e72225752fa588e0685        HEAD
c09b32987380e63e93d93f699e1dbfeae839f8e2        refs/heads/error
f7fe593ca04a5d2a835d0e72225752fa588e0685        refs/heads/master
```

Por último, peguei o registro mais suspeito (`refs/heads/error`) e fui ler ele (não foi preciso baixar, só ler remotamente). Essa parte precisa ter o repositório local ou não vai funcionar.

```bash
git cat-file -p c09b32987380e63e93d93f699e1dbfeae839f8e2
```

Output:

```
RS{se3_that_wasnt_s0_bad_just_som3_git_plumbing}
```

